package listeners;

import android.net.nsd.NsdServiceInfo;

public interface NSDUpdateListener {

    void onServiceResolved(NsdServiceInfo nsdServiceInfo);

}
