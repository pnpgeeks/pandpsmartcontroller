package listeners;

import models.Res_GetDevices;

public interface OnPinStateChangeListener {

    void onPinStateChanged(int position, boolean isOn, Res_GetDevices.Pin pin);

}
