package listeners;

import models.BaseModel;
import okhttp3.Headers;

public interface CallBackListener {

    /**
     * This method will be called by the Controller's on populate method and will redirect
     * the baseModel to the class whose callback listener was passed.
     * @param resModel
     */
    void handleSuccessData(BaseModel resModel);

    /**
     * This method will be called if there was no data received from the server.
     * @param reqModel
     */
    void handleZeroData(BaseModel reqModel);

    /**
     * This method is used to handle the error received from the server.
     * @param errorModel
     */
    void handleErrorDataFromServer(BaseModel errorModel);

    /**
     * Use this method in case there is no internet connection.
     */
    void networkConnectionError();

    /**
     * Use this method when there occurs a problem while connecting to the server.
     */
    void onServerConnectionError();

    /**
     * Use this method if the Response received from the server is malformed.
     * @param reqModel
     */
    void handleInvalidData(BaseModel reqModel);

    /**
     * This method is used to handle the server downtime response.
     */
    void handleServerDownTime();

    /**
     * This method is used to handle the server compulsory app update response
     */
    void handleCompulsoryAppUpdate(Headers headers);

}
