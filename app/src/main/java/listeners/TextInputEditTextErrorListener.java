package listeners;

public interface TextInputEditTextErrorListener {

    void onValidationFailure(int viewId, String message);

}
