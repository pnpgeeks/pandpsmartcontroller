package listeners;

import models.Res_GetDevices;

public interface OnDeviceSelectedListener {

    void onDeviceSelected(int position, Res_GetDevices.Device device);

}
