package retrofit;

import models.Model_DeviceDetails;
import models.Model_PinDetails;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import utils.URLGenerator;

/**
 * This class is the Interface used by Retrofit, which consists of API Callbacks;
 */

public interface APIManager {

    @GET(URLGenerator.TEST_API)
    Call<String> testApi();

    @FormUrlEncoded
    @POST(URLGenerator.API_SIGN_UP)
    Call<String> registerUser(@Field("username") String username,
                              @Field("email") String email,

                              @Field("password") String password);
    @FormUrlEncoded
    @POST(URLGenerator.API_SIGN_IN)
    Call<String> loginUser(@Field("email") String email,
                           @Field("password") String password);

    @POST(URLGenerator.API_ADD_NEW_DEVICE)
    Call<String> addNewDevice(@Body Model_DeviceDetails modelDeviceDetails);

    @FormUrlEncoded
    @POST(URLGenerator.API_GET_DEVICES)
    Call<String> getDevices(@Field("user_id")String userId);

    @POST(URLGenerator.API_UPDATE_PIN_STATE)
    Call<String> updatePinState(@Body Model_PinDetails modelPinDetails);
}
