package retrofit;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import pandp.co.BuildConfig;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import utils.LoggerConfig;
import utils.MySharedPreferences;
import utils.StringDeserializer;
import utils.URLGenerator;
import utils.Util;


public class RetrofitAdapter {

    private String TAG = getClass().getSimpleName();

    private long mTimeOut = 30;
    private APIManager mAPIManager;
    private boolean mIsSelectTimeout;
    private OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
    private Context mContext;

    private String HK_OS = "OS";
    private String HK_OS_VERSION = "OSVERSION";
    private String HK_IMEI = "IMEI";
    private String HK_IPADDRESS = "IPADDRESS";
    private String HK_DEVICE_MODEL = "DEVICEMODEL";
    private String HK_APP_VERSION = "APPVERSION";

    public RetrofitAdapter(Context context, boolean isSelectTimeout) {
        mContext = context;
        mIsSelectTimeout = isSelectTimeout;
    }

    /**
     * This method returns an instance of APIManager(Where all the callback request are written)
     *
     * @return
     */
    public APIManager getAPIManager() {
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(String.class, new StringDeserializer())
                .create();
        if (mAPIManager == null) {
            mAPIManager = new Retrofit.Builder()
                    .baseUrl(URLGenerator.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .client(getOkHttpClient())
                    .build()
                    .create(APIManager.class);

        }
        return mAPIManager;
    }


    private OkHttpClient getOkHttpClient() {

        OkHttpClient okClient = null;
        try {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.HEADERS);
            OkHttpClient.Builder builder = new OkHttpClient.Builder()
                    .addInterceptor(getInterceptor());

//            SSLModel sslModel = generateSSLContext(Constants.SSL_CERTIFICATE_NAME);
//            SSLContext sslContext = sslModel.getSSLContext();
//            X509TrustManager trustManager = sslModel.getTrustManager();
//
//            builder.sslSocketFactory(sslContext.getSocketFactory(), trustManager);

            if (mIsSelectTimeout) {
                Log.e(TAG, "Start Connection");
                builder.connectTimeout(mTimeOut, TimeUnit.SECONDS);
            }
            okClient = builder.build();
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
        return okClient;
    }

    private Interceptor getInterceptor() {
//        String IMEI = MySharedPreferences.getDeviceIMEI(mContext);
        String osVersion = Util.getOSVersion();
        String os = "android_lightapp";
        String ipAddress = Util.getIpAddress();
        String deviceModel = Util.getModel();
        String appVersionName = BuildConfig.VERSION_NAME;


        return chain -> {
            Request request = chain.request().newBuilder()
                    .addHeader(HK_DEVICE_MODEL, deviceModel)
//                    .addHeader(HK_IMEI, IMEI == null ? "" : IMEI)
                    .addHeader(HK_IPADDRESS,ipAddress)
                    .addHeader(HK_OS, os)
                    .addHeader(HK_OS_VERSION, osVersion)
                    .addHeader(HK_APP_VERSION, appVersionName)
                    .build();

            return chain.proceed(request);
        };
    }

    private SSLModel generateSSLContext(String certificateName) {

        SSLModel sslModel = new SSLModel();

        try {
            sslModel.setSSLContext(SSLContext.getInstance("TLS"));
            CertificateFactory cf = CertificateFactory.getInstance("X.509");
            Certificate ca;
            InputStream caInput = new
                    BufferedInputStream(mContext.getAssets().open(certificateName));
            ca = cf.generateCertificate(caInput);
            System.out.println("ca=" + ((X509Certificate) ca).getSubjectDN());


            // Create a KeyStore containing our trusted CAs
            String keyStoreType = KeyStore.getDefaultType();
            KeyStore keyStore = KeyStore.getInstance(keyStoreType);
            keyStore.load(null, null);
            keyStore.setCertificateEntry("ca", ca);

            // Create a TrustManager that trusts the CAs in our KeyStore
            String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
            TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
            tmf.init(keyStore);

            // Create an SSLContext that uses our TrustManager
            sslModel.getSSLContext().init(null, tmf.getTrustManagers(), null);
            sslModel.setTrustManager((X509TrustManager) tmf.getTrustManagers()[0]);

        } catch (Exception e) {
            LoggerConfig.eLog(TAG, "generateSSLContext() -> " + e.getMessage());
        }


        return sslModel;

    }

    private class SSLModel {

        private X509TrustManager mTrustManager;
        private SSLContext mSSLContext;


        X509TrustManager getTrustManager() {
            return mTrustManager;
        }

        void setTrustManager(X509TrustManager trustManager) {
            mTrustManager = trustManager;
        }

        SSLContext getSSLContext() {
            return mSSLContext;
        }

        void setSSLContext(SSLContext SSLContext) {
            mSSLContext = SSLContext;
        }
    }

}
