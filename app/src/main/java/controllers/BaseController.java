package controllers;

import android.app.Dialog;
import android.content.Context;
import android.os.Build;
import androidx.annotation.NonNull;
import android.util.Log;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import fragments.BaseFragment;
import listeners.CallBackListener;
import models.BaseModel;
import models.Model_Error;
import pandp.co.BaseActivity;
import parsers.BaseParser;
import retrofit.APIManager;
import retrofit.RetrofitAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import utils.MyEnum;

import static utils.LoggerConfig.eLog;


public abstract class BaseController {

    private final String TAG = "BaseController";

    //Header Keys.
    private final String HK_SUCCESS = "success";
    private final String HK_SUCCESS_STATUS = "1";
    private final String HK_SUCCESS_DOWNTIME = "-2";
    private final String HK_COMPULSORY_UPDATE = "-1";


    private final String OUTPUT = "OUTPUT";

    //Response status
    private final String RESPONSE_SUCCESS = "success";
    private final String DATA = "data";
    private final int VALID_CODE = 1;
    private final int ERROR_CODE = 0;

    private final int SERVER_CODE = 500;


    MyEnum.ShowProgressbar mShowProgressbar = MyEnum.ShowProgressbar.show;
    CallBackListener mCallBackListener;
    private RetrofitAdapter mRetrofitAdapter;
    BaseModel mReqModel;
    BaseParser mBaseParser;
    APIManager mAPIManager;
    private Context mContext;
    private Dialog mProgressDialog;
    private boolean isTimeout = false;

    /**
     * This method is called in the child class extending BaseController, It passes the parsed
     * json Object to be converted in to the model class.
     *
     * @param objJson
     * @param baseParser
     */
    protected abstract void onPopulate(JSONObject objJson, BaseParser baseParser);


    /**
     * This method implements the CallBack of retrofit in which we would get the Response from the server
     * and whether it was successful or a failure.
     *
     * @param callback
     */
    void callRequestToServer(Call<String> callback) {
        callback.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {

                eLog(TAG, "url: " + response.raw().request().url());

                if (response.isSuccessful()) {
                    if (response.headers() != null &&
                            response.headers().names().size() > 0 &&
                            response.headers().names().contains(HK_SUCCESS)) {
                        String responseStatus = response.headers().get(HK_SUCCESS);
                        if (responseStatus != null) {
                            handleResponseAccordingToStatus(responseStatus, response);
                        } else {
                            //Handle message if the response status is null
                            //Handle invalid response.
                            eLog(TAG, "Response Status is null");
                        }


                    }

                } else if (response.code() == SERVER_CODE) {
                    mCallBackListener.onServerConnectionError();
                    Log.e("Response Error:", response.code() + "," + response.message());
                } else {
                    mCallBackListener.networkConnectionError();
                    Log.e("Response Error:", response.code() + "," + response.message());
                }
                hideProgressBar();
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                hideProgressBar();
                mCallBackListener.networkConnectionError();
                eLog(TAG, "URL: " + call.request().url());
                eLog(TAG, "Failure Error: " + t.getMessage());
            }
        });
    }

    /**
     * This method is used to handle the response According to the status received in header
     * HK_SUCCESS key value.
     *
     * @param responseStatus the status code for different server response
     * @param response       of retrofit api call
     */
    private void handleResponseAccordingToStatus(String responseStatus, Response<String> response) {
        switch (responseStatus) {
            case HK_SUCCESS_STATUS:
                eLog(TAG, "Response Body -> " + response.body());
                parseResponse(createJSONObjectOfResponse(response));
                break;
            case HK_SUCCESS_DOWNTIME:
                //Handle Downtime
                mCallBackListener.handleServerDownTime();
                break;
            case HK_COMPULSORY_UPDATE:
                mCallBackListener.handleCompulsoryAppUpdate(response.headers());
                break;
            default:
                eLog(TAG, "Response Body -> " + response.body());
                parseResponse(createJSONObjectOfResponse(response));
        }

    }

    /**
     * This method returns an JSON Object of the response received from the server. And later it is
     * passed to be parsed and than used in onPopulateMethod()
     *
     * @param response of retrofit api call
     * @return jsonObject of response
     */
    private JSONObject createJSONObjectOfResponse(Response<String> response) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(OUTPUT, new JSONObject(response.body()));
        } catch (Exception e) {
            eLog(TAG, e.getMessage());
        }

        return jsonObject;
    }

    /**
     * This method gets override in the child classes of BaseController in which
     *
     * @param callBackListener is passed having the URL to connect to and
     * @param model            to send along with the request.
     */
    public void startFetching(CallBackListener callBackListener, BaseModel model) {

        eLog(TAG, "Request :" + new Gson().toJson(model));
        mCallBackListener = callBackListener;

        initContext(callBackListener);

        if (mShowProgressbar == MyEnum.ShowProgressbar.show) {
            showProgressBar();
        }
        mRetrofitAdapter = new RetrofitAdapter(mContext, isTimeout);
        mAPIManager = mRetrofitAdapter.getAPIManager();


    }

    private void initContext(CallBackListener callBackListener) {
        if (callBackListener instanceof BaseActivity) {
            BaseActivity baseActivity = (BaseActivity) callBackListener;
            mContext = baseActivity.getApplicationContext();
        } else if (callBackListener instanceof BaseFragment) {
            BaseFragment baseFragment = (BaseFragment) callBackListener;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                mContext = baseFragment.getContext();
            }
        }
    }


    /**
     * This method is used to parse the Response according to the success status received from the
     * response
     *
     * @param jsonObject of response
     */
    private void parseResponse(JSONObject jsonObject) {
        if (jsonObject != null) {
            if (!jsonObject.isNull(OUTPUT)) {
                JSONObject jsonObjectOutput = null;
                try {
                    jsonObjectOutput = jsonObject.getJSONObject(OUTPUT);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (jsonObjectOutput != null && !jsonObjectOutput.isNull(RESPONSE_SUCCESS)) {

                    int responseStatus = jsonObjectOutput.optInt(RESPONSE_SUCCESS);
                    eLog("Success = ", String.valueOf(responseStatus));
                    if (responseStatus == ERROR_CODE || responseStatus == VALID_CODE) {

                        JSONObject objectData = null;
                        try {
                            objectData = jsonObjectOutput.getJSONObject(DATA);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        if (responseStatus == ERROR_CODE && objectData != null) {
                            showErrorFromResponse(objectData);
                        } else {
                            onPopulate(objectData, mBaseParser);
                        }

                    } else {
                        mCallBackListener.handleZeroData(mReqModel);
                    }
                } else {
                    showInvalidData(mReqModel);
                }
            } else {
                showInvalidData(mReqModel);
            }
        }
    }

    private void showErrorFromResponse(JSONObject jsonObject) {
        Model_Error errorModel;
        Gson gson = new Gson();
        errorModel = gson.fromJson(jsonObject.toString(), Model_Error.class);
        mCallBackListener.handleErrorDataFromServer(errorModel);
    }

    private void showInvalidData(BaseModel reqModel) {
        mCallBackListener.handleInvalidData(reqModel);
    }

    private void showProgressBar() {
        /*if (mCallBackListener instanceof BaseActivity) {
            BaseActivity baseActivity = (BaseActivity) mCallBackListener;
            this.mProgressDialog = new Dialog(baseActivity);
        } else if (mCallBackListener instanceof BaseFragment) {
            BaseFragment baseFragment = (BaseFragment) mCallBackListener;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                mContext = baseFragment.getContext();
                if (mContext != null)
                    mProgressDialog = new Dialog(mContext);
            } else {
                mProgressDialog = new Dialog(mContext);
            }
        }
        mProgressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mProgressDialog.setContentView(R.layout.circle_progress);
        mProgressDialog.setCancelable(false);
        if (mProgressDialog.getWindow() != null) {
            mProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            mProgressDialog.getWindow().setGravity(Gravity.CENTER);
        }
        try {
            mProgressDialog.show();
        } catch (Exception e) {
            eLog(TAG, e.toString());
        }*/
    }

    private void hideProgressBar() {
        /*try {
            if (mProgressDialog != null) {
                if (mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                }
            }
        } catch (Exception e) {
            eLog(TAG, "Error in hideProgressBar");
        }*/
    }

}
