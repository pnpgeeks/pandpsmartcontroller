package controllers;

import org.json.JSONObject;

import listeners.CallBackListener;
import models.BaseModel;
import models.Req_UpdatePinState;
import models.Res_UpdatePinState;
import parsers.BaseParser;
import parsers.Parser_UpdatePinState;

public class Controller_UpdateDeviceState extends BaseController {
    @Override
    protected void onPopulate(JSONObject objJson, BaseParser baseParser) {
        Parser_UpdatePinState parserUpdatePinState = (Parser_UpdatePinState) baseParser;
        Res_UpdatePinState resUpdatePinState = (Res_UpdatePinState) parserUpdatePinState.doParsing(objJson);
        mCallBackListener.handleSuccessData(resUpdatePinState);
    }


    @Override
    public void startFetching(CallBackListener callBackListener, BaseModel model) {
        super.startFetching(callBackListener, model);

        mCallBackListener = callBackListener;
        mReqModel = model;
        mBaseParser = new Parser_UpdatePinState();
        Req_UpdatePinState reqUpdatePinState = (Req_UpdatePinState) model;
        callRequestToServer(mAPIManager.updatePinState(reqUpdatePinState.getModelPinDetails()));

    }
}
