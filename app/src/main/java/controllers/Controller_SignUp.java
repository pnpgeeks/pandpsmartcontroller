package controllers;

import org.json.JSONObject;

import listeners.CallBackListener;
import models.BaseModel;
import models.Req_SignUp;
import models.Res_SignUp;
import parsers.BaseParser;
import parsers.Parser_SignUp;

public class Controller_SignUp extends BaseController {

    @Override
    protected void onPopulate(JSONObject objJson, BaseParser baseParser) {
        Parser_SignUp parserSignUp = (Parser_SignUp) baseParser;
        Res_SignUp resSignUp = (Res_SignUp) parserSignUp.doParsing(objJson);
        mCallBackListener.handleSuccessData(resSignUp);
    }

    @Override
    public void startFetching(CallBackListener callBackListener, BaseModel model) {
        super.startFetching(callBackListener, model);

        mCallBackListener = callBackListener;
        mReqModel = model;
        Req_SignUp reqSignUp = (Req_SignUp) model;
        mBaseParser = new Parser_SignUp();
        callRequestToServer(mAPIManager.registerUser(reqSignUp.getUsername(),
                reqSignUp.getEmail(),reqSignUp.getPassword()));

    }
}
