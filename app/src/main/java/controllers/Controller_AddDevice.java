package controllers;

import org.json.JSONObject;

import listeners.CallBackListener;
import models.BaseModel;
import models.Req_AddDevice;
import models.Res_AddDevice;
import parsers.BaseParser;
import parsers.Parser_AddDevice;

public class Controller_AddDevice extends BaseController {

    @Override
    protected void onPopulate(JSONObject objJson, BaseParser baseParser) {
        Parser_AddDevice parserAddDevice = (Parser_AddDevice) baseParser;
        Res_AddDevice resAddDevice = (Res_AddDevice) parserAddDevice.doParsing(objJson);
        mCallBackListener.handleSuccessData(resAddDevice);
    }


    @Override
    public void startFetching(CallBackListener callBackListener, BaseModel model) {
        super.startFetching(callBackListener, model);

        mCallBackListener = callBackListener;
        mReqModel = model;
        mBaseParser = new Parser_AddDevice();
        Req_AddDevice req_addDevice = (Req_AddDevice) model;
        callRequestToServer(mAPIManager.addNewDevice(req_addDevice.getModelDeviceDetails()));

    }
}
