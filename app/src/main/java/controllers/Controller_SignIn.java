package controllers;

import org.json.JSONObject;

import listeners.CallBackListener;
import models.BaseModel;
import models.Req_SignIn;
import models.Res_SignIn;
import parsers.BaseParser;
import parsers.Parser_SignIn;

public class Controller_SignIn extends BaseController {
    @Override
    protected void onPopulate(JSONObject objJson, BaseParser baseParser) {
        Parser_SignIn parserSignIn = (Parser_SignIn) baseParser;
        Res_SignIn resSignIn = (Res_SignIn) parserSignIn.doParsing(objJson);
        mCallBackListener.handleSuccessData(resSignIn);

    }


    @Override
    public void startFetching(CallBackListener callBackListener, BaseModel model) {
        super.startFetching(callBackListener, model);

        mCallBackListener = callBackListener;
        mReqModel = model;
        mBaseParser = new Parser_SignIn();
        Req_SignIn reqSignIn = (Req_SignIn) model;
        callRequestToServer(mAPIManager.loginUser(reqSignIn.getEmail(),
                reqSignIn.getPassword()));
    }
}
