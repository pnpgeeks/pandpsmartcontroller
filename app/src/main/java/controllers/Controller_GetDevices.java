package controllers;

import org.json.JSONObject;

import listeners.CallBackListener;
import models.BaseModel;
import models.Req_GetDevices;
import models.Res_GetDevices;
import parsers.BaseParser;
import parsers.Parser_GetDevices;

public class Controller_GetDevices extends BaseController {
    @Override
    protected void onPopulate(JSONObject objJson, BaseParser baseParser) {
        Parser_GetDevices parserGetDevices = (Parser_GetDevices) baseParser;
        Res_GetDevices resGetDevices = (Res_GetDevices) parserGetDevices.doParsing(objJson);
        mCallBackListener.handleSuccessData(resGetDevices);
    }


    @Override
    public void startFetching(CallBackListener callBackListener, BaseModel model) {
        super.startFetching(callBackListener, model);

        mCallBackListener = callBackListener;
        mReqModel = model;
        mBaseParser = new Parser_GetDevices();
        Req_GetDevices reqGetDevices = (Req_GetDevices) model;
        callRequestToServer(mAPIManager.getDevices(reqGetDevices.getUserId()));
    }
}
