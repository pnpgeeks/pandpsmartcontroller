package controllers;

import org.json.JSONObject;

import listeners.CallBackListener;
import models.BaseModel;
import parsers.BaseParser;

public class Controller_APITest extends BaseController {

    @Override
    protected void onPopulate(JSONObject objJson, BaseParser baseParser) {
        mCallBackListener.handleSuccessData(new BaseModel());
    }


    @Override
    public void startFetching(CallBackListener callBackListener, BaseModel model) {
        super.startFetching(callBackListener, model);
        callRequestToServer(mAPIManager.testApi());
    }
}
