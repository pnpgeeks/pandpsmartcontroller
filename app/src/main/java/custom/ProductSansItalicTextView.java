package custom;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatTextView;
import utils.Constants;

public class ProductSansItalicTextView extends AppCompatTextView {

    public ProductSansItalicTextView(Context context, AttributeSet attributeSet){
        super(context, attributeSet);
        setFonts(context);

    }

    private void setFonts(Context context) {
        this.setTypeface(Typeface.createFromAsset(context.getAssets(),
                String.format("fonts/%s", Constants.FONT_ITALIC)));
    }

}
