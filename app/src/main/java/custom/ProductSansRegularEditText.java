package custom;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import com.google.android.material.textfield.TextInputEditText;

import utils.Constants;

public class ProductSansRegularEditText extends TextInputEditText {

    public ProductSansRegularEditText(Context context) {
        super(context);
        setFonts(context);
    }

    public ProductSansRegularEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ProductSansRegularEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    private void setFonts(Context context) {
        this.setTypeface(Typeface.createFromAsset(context.getAssets(),
                String.format("fonts/%s", Constants.FONT_REGULAR)));
    }
}
