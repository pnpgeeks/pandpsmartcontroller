package realm;

import io.realm.RealmObject;

public class Realm_LoginStatus extends RealmObject {

    private boolean isUserLoggedIn;

    public boolean isUserLoggedIn() {
        return isUserLoggedIn;
    }

    public void setUserLoggedIn(boolean userLoggedIn) {
        isUserLoggedIn = userLoggedIn;
    }
}
