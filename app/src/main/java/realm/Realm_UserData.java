package realm;

import io.realm.RealmObject;

public class Realm_UserData extends RealmObject {

    private int mUserId;
    private String mUsername;
    private String mEmailId;
    private String mPassword;
    private String mRegistrationDate;
    private String mLastModificationDate;
    private String mIsActive;


    public int getUserId() {
        return mUserId;
    }

    public void setUserId(int userId) {
        mUserId = userId;
    }

    public String getUsername() {
        return mUsername;
    }

    public void setUsername(String username) {
        mUsername = username;
    }

    public String getEmailId() {
        return mEmailId;
    }

    public void setEmailId(String emailId) {
        mEmailId = emailId;
    }

    public String getPassword() {
        return mPassword;
    }

    public void setPassword(String password) {
        mPassword = password;
    }

    public String getRegistrationDate() {
        return mRegistrationDate;
    }

    public void setRegistrationDate(String registrationDate) {
        mRegistrationDate = registrationDate;
    }

    public String getLastModificationDate() {
        return mLastModificationDate;
    }

    public void setLastModificationDate(String lastModificationDate) {
        mLastModificationDate = lastModificationDate;
    }

    public String getIsActive() {
        return mIsActive;
    }

    public void setIsActive(String isActive) {
        mIsActive = isActive;
    }
}
