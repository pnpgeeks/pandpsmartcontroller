package parsers;

import com.google.gson.Gson;

import org.json.JSONObject;

import models.BaseModel;
import models.Res_AddDevice;

public class Parser_AddDevice extends BaseParser {
    @Override
    public BaseModel doParsing(JSONObject jsonObject) {
        return new Gson().fromJson(jsonObject.toString(), Res_AddDevice.class);
    }
}
