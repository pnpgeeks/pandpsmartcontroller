package parsers;

import org.json.JSONObject;

import models.BaseModel;


public abstract class BaseParser {

    public abstract BaseModel doParsing(JSONObject jsonObject);
}
