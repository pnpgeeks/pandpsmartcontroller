package parsers;

import com.google.gson.Gson;

import org.json.JSONObject;

import models.BaseModel;
import models.Res_UpdatePinState;

public class Parser_UpdatePinState extends BaseParser {
    @Override
    public BaseModel doParsing(JSONObject jsonObject) {
        return new Gson().fromJson(jsonObject.toString(), Res_UpdatePinState.class);
    }
}
