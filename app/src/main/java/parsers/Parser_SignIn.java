package parsers;

import com.google.gson.Gson;

import org.json.JSONObject;

import models.BaseModel;
import models.Res_SignIn;

public class Parser_SignIn extends BaseParser {

    @Override
    public BaseModel doParsing(JSONObject jsonObject) {
        return new Gson().fromJson(jsonObject.toString(),Res_SignIn.class);
    }
}
