package parsers;

import com.google.gson.Gson;

import org.json.JSONObject;

import models.BaseModel;
import models.Res_GetDevices;

public class Parser_GetDevices extends BaseParser {


    @Override
    public BaseModel doParsing(JSONObject jsonObject) {
        return new Gson().fromJson(jsonObject.toString(), Res_GetDevices.class);
    }
}
