package parsers;

import com.google.gson.Gson;

import org.json.JSONObject;

import models.BaseModel;
import models.Res_SignUp;

public class Parser_SignUp extends BaseParser {


    @Override
    public BaseModel doParsing(JSONObject jsonObject) {
        return new Gson().fromJson(jsonObject.toString(), Res_SignUp.class);
    }
}
