package fragments;

import androidx.fragment.app.Fragment;

import listeners.CallBackListener;
import models.BaseModel;
import okhttp3.Headers;

public abstract class BaseFragment extends Fragment implements CallBackListener {

    @Override
    public abstract void handleSuccessData(BaseModel resModel);

    @Override
    public abstract void handleZeroData(BaseModel reqModel);

    @Override
    public void handleErrorDataFromServer(BaseModel errorModel) {

    }

    @Override
    public void networkConnectionError() {

    }

    @Override
    public void onServerConnectionError() {

    }

    @Override
    public void handleInvalidData(BaseModel reqModel) {

    }

    @Override
    public void handleServerDownTime() {

    }

    @Override
    public void handleCompulsoryAppUpdate(Headers headers) {

    }
}
