package adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import listeners.OnDeviceSelectedListener;
import models.Res_GetDevices;
import pandp.co.R;
import utils.LoggerConfig;

public class Adapter_Device extends RecyclerView.Adapter<Adapter_Device.DeviceViewHolder> {

    private final String TAG = "Adapter_Device";

    private Context mContext;
    private ArrayList<Res_GetDevices.Device> mDevices;
    private OnDeviceSelectedListener mOnDeviceSelectedListener;
    private ArrayList<DeviceViewHolder> mDeviceViewHolders = new ArrayList<>();

    public Adapter_Device(Context context,
                          ArrayList<Res_GetDevices.Device> devices,
                          OnDeviceSelectedListener onDeviceSelectedListener) {
        mContext = context;
        mDevices = devices;
        mOnDeviceSelectedListener = onDeviceSelectedListener;
    }

    @NonNull
    @Override
    public DeviceViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        return new DeviceViewHolder(inflater, parent);
    }

    @Override
    public void onBindViewHolder(@NonNull DeviceViewHolder holder, int position) {

        if (!mDeviceViewHolders.contains(holder)) {
            mDeviceViewHolders.add(holder);
        }

        Res_GetDevices.Device device = mDevices.get(position);

        if (device != null) {
            holder.mDeviceNameTv.setText(device.getDeviceName());


            if (device.mIsDeviceSelected) {
                doSelectDevice(true, holder);
            } else {
                doSelectDevice(false, holder);
            }


            holder.itemView.setOnClickListener(v -> {

                if (!device.mIsDeviceSelected) {
                    resetView();
                    doSelectDevice(true, holder);
                    device.mIsDeviceSelected = true;
                    mOnDeviceSelectedListener.onDeviceSelected(position, device);
                }
            });
        }


    }

    private void resetView() {
        try {
            for (int i = 0; i < mDeviceViewHolders.size(); i++) {
                DeviceViewHolder holder = mDeviceViewHolders.get(i);
                mDevices.get(i).mIsDeviceSelected = false;
                doSelectDevice(false, holder);
            }
        } catch (Exception e) {
            LoggerConfig.eLog(TAG, "resetView:: " + e.getMessage());
        }
    }

    private void doSelectDevice(boolean toggle, DeviceViewHolder holder) {
        if (toggle) {
            holder.mCardViewBg.setCardBackgroundColor(mContext.getResources().
                    getColor(R.color.colorAccent));
        } else {
            holder.mCardViewBg.setCardBackgroundColor(mContext.getResources().
                    getColor(R.color.card_background_color));
        }

    }

    @Override
    public int getItemCount() {
        return mDevices.size();
    }

    class DeviceViewHolder extends RecyclerView.ViewHolder {

        private TextView mDeviceNameTv;
        private CardView mCardViewBg;

        DeviceViewHolder(LayoutInflater layoutInflater, ViewGroup container) {
            super(layoutInflater.inflate(R.layout.list_item_device_tile, container, false));

            mDeviceNameTv = itemView.findViewById(R.id.device_name_tv);
            mCardViewBg = itemView.findViewById(R.id.device_card_bg);
        }


    }
}
