package adapters;

import android.content.Context;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import listeners.OnPinStateChangeListener;
import models.Res_GetDevices;
import pandp.co.R;
import utils.LoggerConfig;

public class Adapter_DevicePin extends RecyclerView.Adapter<Adapter_DevicePin.DevicePinViewHolder> {

    private final String TAG = "Adapter_DevicePin";

    private Context mContext;
    private ArrayList<Res_GetDevices.Pin> mPins;
    private SparseBooleanArray mSparseBooleanArray = new SparseBooleanArray();
    private OnPinStateChangeListener mOnPinStateChangeListener;


    public Adapter_DevicePin(Context context, ArrayList<Res_GetDevices.Pin> pins,
                             OnPinStateChangeListener onPinStateChangeListener) {
        mContext = context;
        mPins = pins;
        mOnPinStateChangeListener = onPinStateChangeListener;
        initSparseBooleanArray();
    }

    private void initSparseBooleanArray() {
        for (int i = 0; i < mPins.size(); i++) {
            mSparseBooleanArray.append(i, false);
        }
    }

    @NonNull
    @Override
    public DevicePinViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        return new DevicePinViewHolder(inflater, parent);
    }

    @Override
    public void onBindViewHolder(@NonNull DevicePinViewHolder holder, int position) {
        Res_GetDevices.Pin pin = mPins.get(position);

        if (pin != null) {

            if (pin.getPinName() != null) {
                holder.mPinTitleTv.setText(pin.getPinName());
            }

            if (pin.getPinState() != null) {
                if (pin.getPinState().equalsIgnoreCase("1")) {
                    holder.mToggleSwitch.setChecked(true);
                } else {
                    holder.mToggleSwitch.setChecked(false);
                }

            }

            holder.mToggleSwitch.setOnCheckedChangeListener((buttonView, isChecked) -> {
                if (isChecked) {
                    mOnPinStateChangeListener.onPinStateChanged(position, true, pin);
                } else {
                    mOnPinStateChangeListener.onPinStateChanged(position, false, pin);
                }

            });

            holder.mMainContentConst.setOnClickListener(v -> {
                if (!mSparseBooleanArray.get(position)) {
                    doShowMoreOptions(true, holder);
                    mSparseBooleanArray.append(position, true);
                } else {
                    doShowMoreOptions(false, holder);
                    mSparseBooleanArray.append(position, false);
                }

            });

        } else {
            LoggerConfig.eLog(TAG, "onBindViewHolder:: pin model is null");
        }


    }

    private void doShowMoreOptions(boolean toggle, DevicePinViewHolder devicePinViewHolder) {
        if (toggle) {
            devicePinViewHolder.mBottomGuidelineView.setVisibility(View.VISIBLE);
            devicePinViewHolder.mMoreOptionsConst.setVisibility(View.VISIBLE);
        } else {
            devicePinViewHolder.mBottomGuidelineView.setVisibility(View.GONE);
            devicePinViewHolder.mMoreOptionsConst.setVisibility(View.GONE);
        }
    }


    @Override
    public int getItemCount() {
        return mPins.size();
    }

    class DevicePinViewHolder extends RecyclerView.ViewHolder {

        private TextView mPinTitleTv;
        private Switch mToggleSwitch;
        private ImageView mLoadIv;
        private LinearLayout mSchedulingLi;
        private LinearLayout mTimerLi;
        private LinearLayout mSettingsLi;
        private ConstraintLayout mMainContentConst;
        private ConstraintLayout mMoreOptionsConst;
        private View mBottomGuidelineView;


        DevicePinViewHolder(LayoutInflater inflater, ViewGroup parent) {
            super(inflater.inflate(R.layout.list_item_device_pin, parent, false));

            mPinTitleTv = itemView.findViewById(R.id.pin_title_tv);
            mToggleSwitch = itemView.findViewById(R.id.toggle_switch);
            mLoadIv = itemView.findViewById(R.id.device_load_img);
            mSchedulingLi = itemView.findViewById(R.id.scheduling_li);
            mTimerLi = itemView.findViewById(R.id.timer_li);
            mSettingsLi = itemView.findViewById(R.id.settings_li);
            mMainContentConst = itemView.findViewById(R.id.main_content_const);
            mBottomGuidelineView = itemView.findViewById(R.id.bottom_guideline);
            mMoreOptionsConst = itemView.findViewById(R.id.more_options_view);

        }


    }

}
