package models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Model_PinDetails extends BaseModel {

    @SerializedName("pin_no")
    @Expose
    private String mPinNo;

    @SerializedName("device_id")
    @Expose
    private String mDeviceId;

    @SerializedName("pin_name")
    @Expose
    private String mPinName;

    @SerializedName("pin_state")
    @Expose
    private String mPinState;


    public String getPinNo() {
        return mPinNo;
    }

    public void setPinNo(String pinNo) {
        mPinNo = pinNo;
    }

    public String getDeviceId() {
        return mDeviceId;
    }

    public void setDeviceId(String deviceId) {
        mDeviceId = deviceId;
    }

    public String getPinName() {
        return mPinName;
    }

    public void setPinName(String pinName) {
        mPinName = pinName;
    }

    public String getPinState() {
        return mPinState;
    }

    public void setPinState(String pinState) {
        mPinState = pinState;
    }
}
