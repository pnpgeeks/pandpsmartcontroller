package models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Model_DeviceDetails extends BaseModel{

    @SerializedName("product_name")
    @Expose
    private String mProductName;

    @SerializedName("device_code")
    @Expose
    private String mDeviceCode;

    @SerializedName("device_mac")
    @Expose
    private String mDeviceMac;

    @SerializedName("device_no_of_pins")
    @Expose
    private String mDevicePins;

    @SerializedName("manufacturing_date")
    @Expose
    private String mManufacturingDate;

    @SerializedName("purchase_date")
    @Expose
    private String mPurchaseDate;

    @SerializedName("user_id")
    @Expose
    private String mUserId;

    @SerializedName("warranty_period")
    @Expose
    private String mWarrantyPeriod;

    @SerializedName("device_name")
    @Expose
    private String mDeviceName;

    @SerializedName("email_id")
    @Expose
    private String mEmail;

    @SerializedName("password")
    @Expose
    private String mPassword;

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public String getPassword() {
        return mPassword;
    }

    public void setPassword(String password) {
        mPassword = password;
    }

    public String getProductName() {
        return mProductName;
    }

    public void setProductName(String productName) {
        mProductName = productName;
    }

    public String getDeviceCode() {
        return mDeviceCode;
    }

    public void setDeviceCode(String deviceCode) {
        mDeviceCode = deviceCode;
    }

    public String getDeviceMac() {
        return mDeviceMac;
    }

    public void setDeviceMac(String deviceMac) {
        mDeviceMac = deviceMac;
    }

    public String getDevicePins() {
        return mDevicePins;
    }

    public void setDevicePins(String devicePins) {
        mDevicePins = devicePins;
    }

    public String getManufacturingDate() {
        return mManufacturingDate;
    }

    public void setManufacturingDate(String manufacturingDate) {
        mManufacturingDate = manufacturingDate;
    }

    public String getPurchaseDate() {
        return mPurchaseDate;
    }

    public void setPurchaseDate(String purchaseDate) {
        mPurchaseDate = purchaseDate;
    }

    public String getUserId() {
        return mUserId;
    }

    public void setUserId(String userId) {
        mUserId = userId;
    }

    public String getWarrantyPeriod() {
        return mWarrantyPeriod;
    }

    public void setWarrantyPeriod(String warrantyPeriod) {
        mWarrantyPeriod = warrantyPeriod;
    }

    public String getDeviceName() {
        return mDeviceName;
    }

    public void setDeviceName(String deviceName) {
        mDeviceName = deviceName;
    }
}
