package models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Res_AddDevice extends BaseModel{

    @SerializedName("result")
    @Expose
    private Integer result;

    public Integer getResult() {
        return result;
    }

    public void setResult(Integer result) {
        this.result = result;
    }

}
