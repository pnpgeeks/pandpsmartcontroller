package models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Res_GetDevices extends BaseModel {

    @SerializedName("devices")
    @Expose
    private ArrayList<Device> devices = null;

    public ArrayList<Device> getDevices() {
        return devices;
    }

    public void setDevices(ArrayList<Device> devices) {
        this.devices = devices;
    }


    public class Device extends BaseModel{

        @SerializedName("deviceId")
        @Expose
        private String deviceId;
        @SerializedName("deviceCode")
        @Expose
        private String deviceCode;
        @SerializedName("deviceName")
        @Expose
        private String deviceName;
        @SerializedName("isDeviceActive")
        @Expose
        private String isDeviceActive;
        @SerializedName("addDateOfDevice")
        @Expose
        private String addDateOfDevice;
        @SerializedName("lastModificationDate")
        @Expose
        private String lastModificationDate;
        @SerializedName("deviceNumberOfPin")
        @Expose
        private String deviceNumberOfPin;
        @SerializedName("manufacturingDate")
        @Expose
        private String manufacturingDate;
        @SerializedName("purchaseDate")
        @Expose
        private String purchaseDate;
        @SerializedName("warrantyPeriod")
        @Expose
        private String warrantyPeriod;
        @SerializedName("deviceMac")
        @Expose
        private String deviceMac;
        @SerializedName("pins")
        @Expose
        private ArrayList<Pin> pins = null;

        public boolean mIsDeviceSelected = false;

        public String getDeviceId() {
            return deviceId;
        }

        public void setDeviceId(String deviceId) {
            this.deviceId = deviceId;
        }

        public String getDeviceCode() {
            return deviceCode;
        }

        public void setDeviceCode(String deviceCode) {
            this.deviceCode = deviceCode;
        }

        public String getDeviceName() {
            return deviceName;
        }

        public void setDeviceName(String deviceName) {
            this.deviceName = deviceName;
        }

        public String getIsDeviceActive() {
            return isDeviceActive;
        }

        public void setIsDeviceActive(String isDeviceActive) {
            this.isDeviceActive = isDeviceActive;
        }

        public String getAddDateOfDevice() {
            return addDateOfDevice;
        }

        public void setAddDateOfDevice(String addDateOfDevice) {
            this.addDateOfDevice = addDateOfDevice;
        }

        public String getLastModificationDate() {
            return lastModificationDate;
        }

        public void setLastModificationDate(String lastModificationDate) {
            this.lastModificationDate = lastModificationDate;
        }

        public String getDeviceNumberOfPin() {
            return deviceNumberOfPin;
        }

        public void setDeviceNumberOfPin(String deviceNumberOfPin) {
            this.deviceNumberOfPin = deviceNumberOfPin;
        }

        public String getManufacturingDate() {
            return manufacturingDate;
        }

        public void setManufacturingDate(String manufacturingDate) {
            this.manufacturingDate = manufacturingDate;
        }

        public String getPurchaseDate() {
            return purchaseDate;
        }

        public void setPurchaseDate(String purchaseDate) {
            this.purchaseDate = purchaseDate;
        }

        public String getWarrantyPeriod() {
            return warrantyPeriod;
        }

        public void setWarrantyPeriod(String warrantyPeriod) {
            this.warrantyPeriod = warrantyPeriod;
        }

        public String getDeviceMac() {
            return deviceMac;
        }

        public void setDeviceMac(String deviceMac) {
            this.deviceMac = deviceMac;
        }

        public ArrayList<Pin> getPins() {
            return pins;
        }

        public void setPins(ArrayList<Pin> pins) {
            this.pins = pins;
        }

    }

    public class Pin extends BaseModel{

        @SerializedName("device_id")
        @Expose
        private String deviceId;
        @SerializedName("pin_no")
        @Expose
        private String pinNo;
        @SerializedName("pin_name")
        @Expose
        private String pinName;
        @SerializedName("pin_state")
        @Expose
        private String pinState;
        @SerializedName("last_modification_time")
        @Expose
        private String lastModificationTime;
        @SerializedName("last_updated_by")
        @Expose
        private String lastUpdatedBy;
        @SerializedName("pin_load_image")
        @Expose
        private String pinLoadImage;

        public String getDeviceId() {
            return deviceId;
        }

        public void setDeviceId(String deviceId) {
            this.deviceId = deviceId;
        }

        public String getPinNo() {
            return pinNo;
        }

        public void setPinNo(String pinNo) {
            this.pinNo = pinNo;
        }

        public String getPinName() {
            return pinName;
        }

        public void setPinName(String pinName) {
            this.pinName = pinName;
        }

        public String getPinState() {
            return pinState;
        }

        public void setPinState(String pinState) {
            this.pinState = pinState;
        }

        public String getLastModificationTime() {
            return lastModificationTime;
        }

        public void setLastModificationTime(String lastModificationTime) {
            this.lastModificationTime = lastModificationTime;
        }

        public String getLastUpdatedBy() {
            return lastUpdatedBy;
        }

        public void setLastUpdatedBy(String lastUpdatedBy) {
            this.lastUpdatedBy = lastUpdatedBy;
        }

        public String getPinLoadImage() {
            return pinLoadImage;
        }

        public void setPinLoadImage(String pinLoadImage) {
            this.pinLoadImage = pinLoadImage;
        }

    }


}
