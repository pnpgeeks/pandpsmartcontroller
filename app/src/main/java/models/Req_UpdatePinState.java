package models;

public class Req_UpdatePinState extends BaseModel {


    private Model_PinDetails mModelPinDetails;


    public Model_PinDetails getModelPinDetails() {
        return mModelPinDetails;
    }

    public void setModelPinDetails(Model_PinDetails modelPinDetails) {
        mModelPinDetails = modelPinDetails;
    }
}
