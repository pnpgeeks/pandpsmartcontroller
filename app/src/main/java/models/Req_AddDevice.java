package models;

public class Req_AddDevice extends BaseModel {

    private Model_DeviceDetails mModelDeviceDetails;
    private String mEmail;
    private String mPassword;


    public Model_DeviceDetails getModelDeviceDetails() {
        return mModelDeviceDetails;
    }

    public void setModelDeviceDetails(Model_DeviceDetails modelDeviceDetails) {
        mModelDeviceDetails = modelDeviceDetails;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public String getPassword() {
        return mPassword;
    }

    public void setPassword(String password) {
        mPassword = password;
    }
}
