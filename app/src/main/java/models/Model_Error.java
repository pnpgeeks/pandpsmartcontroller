package models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Model_Error extends BaseModel {

    @SerializedName("errorCode")
    @Expose
    private Integer mErrorCode;
    @SerializedName("error")
    @Expose
    private String mError;
    @SerializedName("title")
    @Expose
    private String mTitle;

    public Integer getErrorCode() {
        return mErrorCode;
    }

    public void setErrorCode(Integer errorCode) {
        mErrorCode = errorCode;
    }

    public String getError() {
        return mError;
    }

    public void setError(String error) {
        mError = error;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }


}
