package pandp.co;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.google.android.material.textfield.TextInputLayout;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import controllers.Controller_SignIn;
import realm.Realm_LoginStatus;
import custom.ProductSansRegularEditText;
import custom.ProductSansRegularTextView;
import io.realm.Realm;
import models.BaseModel;
import models.Req_SignIn;
import models.Res_SignIn;
import realm.Realm_UserData;
import utils.Constants;

public class Activity_SignIn extends BaseActivity {

    @BindView(R.id.logo_iv)
    ImageView mLogoIv;

    @Email
    @NotEmpty
    @BindView(R.id.email_edit_text)
    ProductSansRegularEditText mEmailEditText;
    @BindView(R.id.email_til)
    TextInputLayout mEmailTil;

    @Password(min = 8)
    @NotEmpty
    @BindView(R.id.password_edit_text)
    ProductSansRegularEditText mPasswordEditText;
    @BindView(R.id.password_til)
    TextInputLayout mPasswordTil;

    @BindView(R.id.sign_in_button)
    Button mSignInButton;
    @BindView(R.id.sign_up_tv)
    ProductSansRegularTextView mSignUpTv;


    private Realm mRealm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mRealm = getRealm(this);
        redirectIfLoggedIn();

        setContentView(R.layout.activity_signin);
        ButterKnife.bind(this);


        initSoftKeyboardListener(mLogoIv);


        initSignUpClickable();

        setEditTextWatchers();

        initValidationListener(this);

    }

    private void redirectIfLoggedIn() {

        if (mRealm != null) {
            Realm_LoginStatus realmLoginStatus = mRealm.where(Realm_LoginStatus.class).findFirst();
            if (realmLoginStatus != null) {
                if (realmLoginStatus.isUserLoggedIn()) {
                    startActivity(new Intent(this, Activity_HomeScreen.class));
                    finishAffinity();
                }

            }
        }


    }

    private void initSignUpClickable() {
        mSignUpTv.setText(createSpannableString());
        mSignUpTv.setMovementMethod(LinkMovementMethod.getInstance());
        mSignUpTv.setHighlightColor(Color.TRANSPARENT);
        mSignUpTv.setEnabled(true);
    }

    @Override
    public void handleSuccessData(BaseModel resModel) {

        if (resModel != null) {
            if (resModel instanceof Res_SignIn) {
                Res_SignIn resSignIn = (Res_SignIn) resModel;

                if (mPasswordEditText.getText() != null) {
                    if (mPasswordEditText.getText().toString().equals(resSignIn.getPassword())) {
                        updateLoginStatus(true);
                        storeUserDetails(resSignIn);
                        redirectIfLoggedIn();
                    }
                }
            }
        }

    }


    private void updateLoginStatus(boolean isLoggedIn) {

        if (mRealm != null) {
            if (mRealm.where(Realm_LoginStatus.class).findAll().size() > 0) {
                mRealm.beginTransaction();
                mRealm.delete(Realm_LoginStatus.class);
                mRealm.commitTransaction();
            }
            mRealm.beginTransaction();
            Realm_LoginStatus realmLoginStatus = new Realm_LoginStatus();
            realmLoginStatus.setUserLoggedIn(isLoggedIn);
            mRealm.insertOrUpdate(realmLoginStatus);
            mRealm.commitTransaction();

        }
    }

    private void storeUserDetails(Res_SignIn resSignIn) {

        if (mRealm != null) {

            if (mRealm.where(Realm_UserData.class).findAll().size() > 0) {
                mRealm.beginTransaction();
                mRealm.delete(Realm_UserData.class);
                mRealm.commitTransaction();
            }


            Realm_UserData realmUserData = new Realm_UserData();
            realmUserData.setEmailId(resSignIn.getEmailId());
            realmUserData.setIsActive(resSignIn.getIsActive());
            realmUserData.setLastModificationDate(resSignIn.getLastModificationDate());
            realmUserData.setPassword(resSignIn.getPassword());
            realmUserData.setRegistrationDate(resSignIn.getRegistrationDate());
            realmUserData.setUserId(Integer.valueOf(resSignIn.getUserId()));
            realmUserData.setUsername(resSignIn.getUsername());
            mRealm.beginTransaction();
            mRealm.insertOrUpdate(realmUserData);
            mRealm.commitTransaction();

        }


    }

    @Override
    public void handleZeroData(BaseModel reqModel) {

    }


    private void setEditTextWatchers() {

        mEmailEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                disableErrorView(mEmailTil);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mPasswordEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                disableErrorView(mPasswordTil);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    private SpannableString createSpannableString() {
        Spannable spannable = new SpannableString(getString(R.string.dont_have_a_account));
        String str = spannable.toString();
        int iStart = str.indexOf(getString(R.string.sign_up));
        int iEnd = iStart + getString(R.string.sign_up).length();

        SpannableString ssText = new SpannableString(spannable);
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(@NonNull View widget) {
                startActivityForResult(new Intent(Activity_SignIn.this, Activity_SignUp.class),
                        Constants.INTENT_FOR_SIGN_UP);

            }

            @Override
            public void updateDrawState(@NonNull TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(true);
                ds.setColor(getResources().getColor(R.color.colorAccent));
            }
        };
        ssText.setSpan(clickableSpan, iStart, iEnd, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        return ssText;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {

            if (requestCode == Constants.INTENT_FOR_SIGN_UP) {
                //Do nasty things here.


            }


        }

    }

    @Override
    public void onValidationSucceeded() {
        signInUser();
    }


    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        onValidationFailure(errors);
    }

    @OnClick(R.id.sign_in_button)
    public void onViewClicked() {
        mValidator.validate();
    }

    @Override
    public void onValidationFailure(int viewId, String message) {
        switch (viewId) {
            case R.id.email_edit_text:
                enableErrorView(mEmailTil);
                mEmailTil.setError(message);
                break;
            case R.id.password_edit_text:
                enableErrorView(mPasswordTil);
                mPasswordTil.setError(message);
                break;
        }
    }

    private void signInUser() {

        Controller_SignIn controllerSignIn = new Controller_SignIn();
        Req_SignIn reqSignIn = new Req_SignIn();
        if (mEmailEditText.getText() != null) {
            reqSignIn.setEmail(mEmailEditText.getText().toString().trim());
        }

        if (mPasswordEditText.getText() != null) {
            reqSignIn.setPassword(mPasswordEditText.getText().toString().trim());
        }

        controllerSignIn.startFetching(this, reqSignIn);

    }
}

