package pandp.co;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.zxing.Result;

import org.json.JSONObject;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import me.dm7.barcodescanner.zxing.ZXingScannerView;
import utils.Constants;
import utils.LoggerConfig;

public class Activity_ScanQRCode extends AppCompatActivity implements ZXingScannerView.ResultHandler {

    private static final String TAG = "Activity_ScanQRCode";


    private ZXingScannerView mScannerView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mScannerView = new ZXingScannerView(this);
        mScannerView.setLaserEnabled(false);
        mScannerView.setAutoFocus(true);
        setContentView(mScannerView);

    }

    @Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this);
        mScannerView.startCamera();
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();
    }


    @Override
    public void handleResult(Result rawResult) {
        Log.v(TAG, rawResult.getText());
        Log.v(TAG, rawResult.getBarcodeFormat().toString());

        parseScannedResult(rawResult);
    }

    private void parseScannedResult(Result rawResult){

        try {
            JSONObject deviceDetailsJson = new JSONObject(rawResult.getText());

            if (deviceDetailsJson.optString("product_name") != null) {
                if (deviceDetailsJson.optString("product_name").equals("P&PSmart")) {
                    Intent intent = new Intent();
                    intent.putExtra(Constants.EXTRA_DEVICE_DETAILS_JSON, rawResult.getText());
                    setResult(RESULT_OK, intent);
                    finish();
                } else {
                    mScannerView.resumeCameraPreview(this);
                    Toast.makeText(getApplicationContext(), "Invalid QR Code",
                            Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(getApplicationContext(), "Invalid QR Code",
                        Toast.LENGTH_SHORT).show();
                mScannerView.resumeCameraPreview(this);
            }

        } catch (Exception e) {
            LoggerConfig.eLog(TAG, "handleResult()::Error while parsing scanned QR Code");
        }

    }


}
