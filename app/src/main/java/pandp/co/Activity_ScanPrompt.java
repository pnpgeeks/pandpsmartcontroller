package pandp.co;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.mobsandgeeks.saripaar.ValidationError;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import models.BaseModel;
import utils.Constants;

public class Activity_ScanPrompt extends BaseActivity {

    @BindView(R.id.scan_button)
    Button mScanButton;
    @BindView(R.id.back_button_iv)
    ImageView mBackButtonIv;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_prompt);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.scan_button, R.id.back_button_iv})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.scan_button:
                if (checkPermissionForCamera()) {
                    startActivityForResult(new Intent(this, Activity_ScanQRCode.class),
                            Constants.INTENT_START_ACTIVITY_SCAN_QR_CODE);
                } else {
                    requestPermissionRationaleForCamera();
                }
                break;
            case R.id.back_button_iv:
                onBackPressed();
                break;
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case Constants.CAMERA_PERMISSION_REQUEST_CODE:

                boolean cameraPermissionGranted = true;

                for (int grantResult : grantResults) {
                    cameraPermissionGranted &= grantResult == PackageManager.PERMISSION_GRANTED;
                }

                if (cameraPermissionGranted) {
                    startActivity(new Intent(this, Activity_ScanQRCode.class));
                } else {
                    openSettingsToGivePermission(getString(R.string.camera_permission_disabled),
                            getString(R.string.permission_error_message_camera),
                            Constants.CAMERA_PERMISSION_IN_SETTINGS);
                }
                break;
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == RESULT_OK){

            if(requestCode == Constants.INTENT_START_ACTIVITY_SCAN_QR_CODE){
                if(data != null && data.getStringExtra(Constants.EXTRA_DEVICE_DETAILS_JSON)!= null){
                    setResult(RESULT_OK,data);
                    finish();
                }
            }
        }
    }

    @Override
    public void handleSuccessData(BaseModel resModel) {

    }

    @Override
    public void handleZeroData(BaseModel reqModel) {

    }

    @Override
    public void onValidationSucceeded() {

    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {

    }

    @Override
    public void onValidationFailure(int viewId, String message) {

    }

}
