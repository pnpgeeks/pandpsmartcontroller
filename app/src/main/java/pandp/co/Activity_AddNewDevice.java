package pandp.co;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.Min;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;

import java.util.List;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import controllers.Controller_AddDevice;
import custom.ProductSansRegularEditText;
import models.BaseModel;
import models.Model_DeviceDetails;
import models.Req_AddDevice;
import models.Res_AddDevice;
import utils.Constants;
import utils.LoggerConfig;

public class Activity_AddNewDevice extends BaseActivity {

    private final String TAG = "Activity_AddNewDevice";

    @BindView(R.id.back_button_iv)
    ImageView mBackButtonIv;
    @BindView(R.id.logo_bg)
    ConstraintLayout mLogoBg;

    @NotEmpty
    @BindView(R.id.device_name_edit_text)
    ProductSansRegularEditText mDeviceNameEditText;
    @BindView(R.id.device_name_til)
    TextInputLayout mDeviceNameTil;

    @BindView(R.id.device_code_edit_text)
    ProductSansRegularEditText mDeviceCodeEditText;
    @BindView(R.id.device_code_til)
    TextInputLayout mDeviceCodeTil;
    @BindView(R.id.scan_qr_code_btn)
    ImageView mScanQrCodeBtn;

    @Email
    @NotEmpty
    @BindView(R.id.email_edit_text)
    ProductSansRegularEditText mEmailEditText;
    @BindView(R.id.email_til)
    TextInputLayout mEmailTil;

    @Password(min = 8)
    @NotEmpty
    @BindView(R.id.password_edit_text)
    ProductSansRegularEditText mPasswordEditText;
    @BindView(R.id.password_til)
    TextInputLayout mPasswordTil;
    @BindView(R.id.add_new_device_button)
    Button mSignUpButton;

    private Model_DeviceDetails mModelDeviceDetails;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_device);
        ButterKnife.bind(this);

        initValidationListener(this);

        setEditTextWatchers();

        initSoftKeyboardListener(mLogoBg);

    }

    private void setEditTextWatchers() {

        mEmailEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                disableErrorView(mEmailTil);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mPasswordEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                disableErrorView(mPasswordTil);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mDeviceCodeEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                disableErrorView(mDeviceCodeTil);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mDeviceNameEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                disableErrorView(mDeviceNameTil);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }


    @Override
    public void handleSuccessData(BaseModel resModel) {

        if (resModel != null) {
            if (resModel instanceof Res_AddDevice) {
                Res_AddDevice resAddDevice = (Res_AddDevice) resModel;
                if (resAddDevice.getResult() == 1) {
                    Toast.makeText(getApplicationContext(), "Device added successfully",
                            Toast.LENGTH_SHORT).show();
                    finish();

                }

            }
        }


    }

    @Override
    public void handleZeroData(BaseModel reqModel) {

    }

    @Override
    public void onValidationSucceeded() {
        requestToAddNewDevice();
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        onValidationFailure(errors);

    }

    @Override
    public void onValidationFailure(int viewId, String message) {
        switch (viewId) {
            case R.id.email_edit_text:
                enableErrorView(mEmailTil);
                mEmailTil.setError(message);
                break;
            case R.id.password_edit_text:
                enableErrorView(mPasswordTil);
                mPasswordTil.setError(message);
                break;
            case R.id.device_name_edit_text:
                enableErrorView(mDeviceNameTil);
                mDeviceNameTil.setError(message);
                break;
            case R.id.device_code_edit_text:
                enableErrorView(mDeviceCodeTil);
                mDeviceCodeTil.setError(message);
                break;
        }

    }

    @OnClick({R.id.back_button_iv, R.id.scan_qr_code_btn, R.id.add_new_device_button})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.back_button_iv:
                onBackPressed();
                break;
            case R.id.scan_qr_code_btn:
                startActivityForResult(new Intent(this, Activity_ScanPrompt.class),
                        Constants.INTENT_START_ACTIVITY_SCAN_PROMPT);
                break;
            case R.id.add_new_device_button:
                mValidator.validate();
                break;
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {

            if (requestCode == Constants.INTENT_START_ACTIVITY_SCAN_PROMPT) {
                if (data != null && data.getStringExtra(Constants.EXTRA_DEVICE_DETAILS_JSON) != null) {
                    String deviceDetailsJson = data.getStringExtra(Constants.EXTRA_DEVICE_DETAILS_JSON);
                    mModelDeviceDetails = new Gson().fromJson(deviceDetailsJson, Model_DeviceDetails.class);
                    if (mModelDeviceDetails != null) {
                        if (mModelDeviceDetails.getDeviceCode().trim().length() == 10) {
                            mDeviceCodeEditText.setText(mModelDeviceDetails.getDeviceCode());
                            if(mDeviceCodeEditText.getText() != null) {
                                mDeviceCodeEditText.setSelection(mDeviceCodeEditText.getText().toString().length());
                            }
                        } else {
                            LoggerConfig.eLog(TAG, "onActivityResult():: invalid device code");
                        }
                    }
                    LoggerConfig.eLog(TAG, "onActivityResult():: model_device_details is null");
                }
            }


        }

    }

    private void requestToAddNewDevice() {
        if (mModelDeviceDetails != null) {
            if (mDeviceNameEditText.getText() != null) {
                mModelDeviceDetails.setDeviceName(mDeviceNameEditText.getText().toString());
            } else {
                mModelDeviceDetails.setDeviceName("New Device");
            }
            mModelDeviceDetails.setUserId(getUserId());

            if(mEmailEditText.getText() != null) {
                mModelDeviceDetails.setEmail(mEmailEditText.getText().toString());
            }

            if(mPasswordEditText.getText() != null){
                mModelDeviceDetails.setPassword(mPasswordEditText.getText().toString());
            }

            Controller_AddDevice controllerAddDevice = new Controller_AddDevice();
            Req_AddDevice reqAddDevice = new Req_AddDevice();
            reqAddDevice.setModelDeviceDetails(mModelDeviceDetails);
            controllerAddDevice.startFetching(this, reqAddDevice);

        } else {
            Toast.makeText(getApplicationContext(),
                    "Please scan QRCode First", Toast.LENGTH_SHORT).show();
        }


    }
}
