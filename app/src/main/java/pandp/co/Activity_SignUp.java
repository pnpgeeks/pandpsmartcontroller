package pandp.co;

import android.app.AlertDialog;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.google.android.material.textfield.TextInputLayout;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.annotation.ConfirmPassword;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;

import java.util.List;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import controllers.Controller_SignUp;
import custom.ProductSansRegularEditText;
import models.BaseModel;
import models.Req_SignUp;
import models.Res_SignUp;

public class Activity_SignUp extends BaseActivity{

    @BindView(R.id.back_button_iv)
    ImageView mBackButtonIv;
    @BindView(R.id.logo_bg)
    ConstraintLayout mLogoBg;

    @NotEmpty
    @BindView(R.id.username_edit_text)
    ProductSansRegularEditText mUsernameEditText;
    @BindView(R.id.username_til)
    TextInputLayout mUsernameTil;

    @Email
    @NotEmpty
    @BindView(R.id.email_edit_text)
    ProductSansRegularEditText mEmailEditText;
    @BindView(R.id.email_til)
    TextInputLayout mEmailTil;

    @Password(min = 8)
    @NotEmpty
    @BindView(R.id.password_edit_text)
    ProductSansRegularEditText mPasswordEditText;
    @BindView(R.id.password_til)
    TextInputLayout mPasswordTil;

    @ConfirmPassword
    @NotEmpty
    @BindView(R.id.confirm_password_edit_text)
    ProductSansRegularEditText mConfirmPasswordEditText;
    @BindView(R.id.confirm_password_til)
    TextInputLayout mConfirmPasswordTil;

    @BindView(R.id.sign_up_button)
    Button mSignUpButton;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        ButterKnife.bind(this);

        initValidationListener(this);

        setEditTextWatchers();

        initSoftKeyboardListener(mLogoBg);

    }


    private void setEditTextWatchers(){

        mEmailEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                disableErrorView(mEmailTil);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mPasswordEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                disableErrorView(mPasswordTil);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mConfirmPasswordEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                disableErrorView(mConfirmPasswordTil);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        mUsernameEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                disableErrorView(mUsernameTil);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    @Override
    public void handleSuccessData(BaseModel resModel) {

        if(resModel != null){
            if(resModel instanceof Res_SignUp){
                Res_SignUp resSignUp = (Res_SignUp) resModel;
                if(resSignUp.getResult() == 1){
                    showVerificationEmailDialogBox();
                }

            }
        }

    }

    private void showVerificationEmailDialogBox() {
        new AlertDialog.Builder(this).setTitle("Email Verification")
                .setMessage("To activate your account please follow the instructions provided to you in a email sent to you on your given email address.")
                .setPositiveButton("Ok",(dialog,which) -> {
                   setResult(RESULT_OK);
                   finish();
                })
                .setCancelable(false)
                .create().show();
    }

    @Override
    public void handleZeroData(BaseModel reqModel) {

    }

    @OnClick({R.id.back_button_iv, R.id.sign_up_button})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.back_button_iv:
                onBackPressed();
                break;
            case R.id.sign_up_button:
                mValidator.validate();
                break;
        }
    }

    @Override
    public void onValidationSucceeded() {
        sendRequestToSignUp();
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        onValidationFailure(errors);

    }


    @Override
    public void onValidationFailure(int viewId, String message) {
        switch (viewId){
            case R.id.email_edit_text:
                enableErrorView(mEmailTil);
                mEmailTil.setError(message);
                break;
            case R.id.password_edit_text:
                enableErrorView(mPasswordTil);
                mPasswordTil.setError(message);
                break;
            case R.id.confirm_password_edit_text:
                enableErrorView(mConfirmPasswordTil);
                mConfirmPasswordTil.setError(message);
                break;
            case R.id.username_edit_text:
                enableErrorView(mUsernameTil);
                mUsernameTil.setError(message);
                break;


        }
    }

    private void sendRequestToSignUp(){
        Controller_SignUp controllerSignUp = new Controller_SignUp();
        Req_SignUp reqSignUp = new Req_SignUp();
        if(mEmailEditText.getText() != null) {
            reqSignUp.setEmail(mEmailEditText.getText().toString().trim());
        }

        if(mUsernameEditText.getText() != null){
            reqSignUp.setUsername(mUsernameEditText.getText().toString().trim());
        }

        if(mPasswordEditText.getText() != null){
            reqSignUp.setPassword(mPasswordEditText.getText().toString().trim());
        }

        controllerSignUp.startFetching(this,reqSignUp);


    }
}
