package pandp.co;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Rect;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.nsd.NsdServiceInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.provider.Settings;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;

import java.util.List;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import listeners.CallBackListener;
import listeners.NSDUpdateListener;
import listeners.TextInputEditTextErrorListener;
import models.BaseModel;
import models.Model_Error;
import okhttp3.Headers;
import realm.Realm_UserData;
import singleton.ControllerDataSingleton;
import utils.Constants;
import utils.DialogBox;
import utils.InternetCheck;
import utils.LoggerConfig;
import utils.NsdHelper;
import utils.WifiConnectionChangeReceiver;

public abstract class BaseActivity extends AppCompatActivity implements CallBackListener,
        Validator.ValidationListener, TextInputEditTextErrorListener, NSDUpdateListener {

    private final String TAG = "BaseActivity";

    protected Validator mValidator;
    protected InternetCheck mInternetCheck;
    protected NsdHelper mNsdHelper;
    protected ControllerDataSingleton mControllerDataSingleton;
    protected WifiConnectionChangeReceiver mWifiConnectionChangeReceiver;

    {
        mControllerDataSingleton = ControllerDataSingleton.getInstance();
    }

    protected void initValidationListener(Context context) {
        mValidator = new Validator(context);
        mValidator.setValidationListener(this);
    }


    @Override
    public abstract void handleSuccessData(BaseModel resModel);

    @Override
    public abstract void handleZeroData(BaseModel reqModel);

    @Override
    public void handleErrorDataFromServer(BaseModel errorModel) {
        Model_Error modelError = (Model_Error) errorModel;

        DialogBox.showDialog(this, modelError.getTitle(), modelError.getError());

    }

    @Override
    public void networkConnectionError() {
        DialogBox.showDialog(this, getString(R.string.network_error), getString(R.string.check_your_internet_connection));
    }

    @Override
    public void onServerConnectionError() {
        DialogBox.showDialog(this, getString(R.string.server_error), getString(R.string.check_server_connection));
    }

    @Override
    public void handleInvalidData(BaseModel reqModel) {

    }

    @Override
    public void handleServerDownTime() {
        DialogBox.showDialog(this, getString(R.string.downtime), getString(R.string.server_is_currently_facing_downtime));
    }

    @Override
    public void handleCompulsoryAppUpdate(Headers headers) {

    }

    /**
     * Method used to check whether camera permission is given or not
     *
     * @return boolean
     */
    public boolean checkPermissionForCamera() {
        int resultCamera = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        return resultCamera == PackageManager.PERMISSION_GRANTED;

    }

    public void requestPermissionRationaleForCamera() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)) {
            showRationalePermissionCamDialogBox();
        } else {
            requestCameraPermission();
        }
    }

    public void requestCameraPermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA},
                Constants.CAMERA_PERMISSION_REQUEST_CODE);

    }

    public void showRationalePermissionCamDialogBox() {
        new AlertDialog.Builder(this)
                .setMessage(getString(R.string.camera_permission_rationale))
                .setCancelable(false)
                .setPositiveButton(getString(R.string.give_permission),
                        (dialog, which) -> requestCameraPermission())
                .create().show();

    }

    /**
     * This method is called to open the Settings screen.
     *
     * @param title       of dialog
     * @param message     of dialog
     * @param requestCode for start activity result
     */
    public void openSettingsToGivePermission(String title, String message, final int requestCode) {
        new AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton(getString(R.string.settings),
                        (dialog, which) -> openSettings(requestCode)).create().show();
    }


    /**
     * This method is used to open settings app with a intent
     *
     * @param requestCode for activity result
     */
    private void openSettings(int requestCode) {
        Intent intent = new Intent();
        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, requestCode);
    }


    /**
     * This method is used to hide or show particular views when keyboard is opened or closed
     *
     * @param views varags of view.
     */
    protected void initSoftKeyboardListener(View... views) {
        final int MIN_KEYBOARD_HEIGHT_PX = 150;
        final View decorView = getWindow().getDecorView();
        decorView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            private final Rect windowVisibleDisplayFrame = new Rect();
            private int lastVisibleDecorViewHeight;

            @Override
            public void onGlobalLayout() {
                decorView.getWindowVisibleDisplayFrame(windowVisibleDisplayFrame);
                final int visibleDecorViewHeight = windowVisibleDisplayFrame.height();


                if (lastVisibleDecorViewHeight != 0) {
                    if (lastVisibleDecorViewHeight > visibleDecorViewHeight + MIN_KEYBOARD_HEIGHT_PX) {
                        for (View view : views) {
                            view.setVisibility(View.GONE);
                        }
                    } else if (lastVisibleDecorViewHeight + MIN_KEYBOARD_HEIGHT_PX < visibleDecorViewHeight) {
                        for (View view : views) {
                            view.setVisibility(View.VISIBLE);
                        }

                    }
                }
                lastVisibleDecorViewHeight = visibleDecorViewHeight;

            }
        });
    }

    /**
     * This method is used to populate the Edit text with errors on validation failure
     *
     * @param errors list of possible errors
     */
    public void onValidationFailure(List<ValidationError> errors) {

        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof TextInputEditText) {
                onValidationFailure(view.getId(), message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }

    }

    public void disableErrorView(TextInputLayout... textInputLayouts) {
        for (TextInputLayout textInputLayout : textInputLayouts) {
            textInputLayout.setError(null);
            textInputLayout.setErrorEnabled(false);
        }
    }

    public void enableErrorView(TextInputLayout... textInputLayouts) {
        for (TextInputLayout textInputLayout : textInputLayouts) {
            textInputLayout.setErrorEnabled(true);
        }
    }


    protected Realm getRealm(Context context) {
        Realm.init(context);
        RealmConfiguration config = new RealmConfiguration.Builder()
                .deleteRealmIfMigrationNeeded()
                .build();

        return Realm.getInstance(config);
    }

    protected String getUserId() {
        Realm realm = getRealm(this);

        if (realm != null) {

            Realm_UserData realmUserData = realm.where(Realm_UserData.class).findFirst();

            if (realmUserData != null) {
                return String.valueOf(realmUserData.getUserId());
            }

        }

        return null;


    }

    protected boolean isWifiConnected() {

        WifiManager wifiMgr = (WifiManager) getApplicationContext().
                getSystemService(Context.WIFI_SERVICE);

        if (wifiMgr.isWifiEnabled()) {

            WifiInfo wifiInfo = wifiMgr.getConnectionInfo();

            return wifiInfo.getNetworkId() != -1;
        } else {
            return false;
        }

    }


    protected boolean isMobileNetConnected() {
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedMobile;

    }

    protected void startNSDServiceDiscovery() {
        mNsdHelper = new NsdHelper(this, this);
        mNsdHelper.initializeNsd();
        mNsdHelper.discoverServices();
    }

    @Override
    public void onServiceResolved(NsdServiceInfo nsdServiceInfo) {
        LoggerConfig.eLog(TAG, "onServiceResolved::called");

        if (nsdServiceInfo != null) {
            mControllerDataSingleton.setHostName(String.valueOf(nsdServiceInfo.getHost()));
            mControllerDataSingleton.setPort(String.valueOf(nsdServiceInfo.getPort()));
            mControllerDataSingleton.setServiceName(nsdServiceInfo.getServiceName());
            mControllerDataSingleton.setServiceType(nsdServiceInfo.getServiceType());

            Toast.makeText(getApplicationContext(),
                    nsdServiceInfo.getHost() + "", Toast.LENGTH_SHORT).show();
            mNsdHelper.stopDiscovery();
        }

    }

    protected void registerWifiStateChangeReceiver() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        mWifiConnectionChangeReceiver = new WifiConnectionChangeReceiver();
        registerReceiver(mWifiConnectionChangeReceiver, intentFilter);
    }

    protected void unregisterWifiStateChangeReceiver() {
        if (mWifiConnectionChangeReceiver != null) {
            unregisterReceiver(mWifiConnectionChangeReceiver);
        } else {
            LoggerConfig.eLog(TAG,
                    "unregisterWifiStateChangeReceiver:: WifiConnectionChangeReceiver is null");
        }
    }
}
