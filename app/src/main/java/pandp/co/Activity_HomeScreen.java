package pandp.co;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.ValidationError;

import java.util.List;

import adapters.Adapter_Device;
import adapters.Adapter_DevicePin;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import controllers.Controller_GetDevices;
import controllers.Controller_UpdateDeviceState;
import listeners.OnDeviceSelectedListener;
import listeners.OnPinStateChangeListener;
import models.BaseModel;
import models.Model_PinDetails;
import models.Req_GetDevices;
import models.Req_UpdatePinState;
import models.Res_GetDevices;
import models.Res_UpdatePinState;
import singleton.DevicePinDataSingleton;
import utils.Constants;
import utils.InternetCheck;
import utils.LoggerConfig;


public class Activity_HomeScreen extends BaseActivity implements OnDeviceSelectedListener,
        OnPinStateChangeListener {

    private final String TAG = "Activity_HomeScreen";

    @BindView(R.id.add_device_iv)
    ImageView mAddDeviceIv;
    @BindView(R.id.no_device_li)
    LinearLayout mNoDeviceLi;
    @BindView(R.id.devices_recycler_view)
    RecyclerView mDevicesRecyclerView;
    @BindView(R.id.device_pins_recycler_view)
    RecyclerView mDevicePinsRecyclerView;

    private DevicePinDataSingleton mDevicePinDataSingleton;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homescreen);
        ButterKnife.bind(this);

        mDevicePinDataSingleton = DevicePinDataSingleton.getInstance();

        mDevicesRecyclerView.setLayoutManager(new LinearLayoutManager(this,
                RecyclerView.HORIZONTAL,
                false));

        mDevicePinsRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        if (isWifiConnected() || isMobileNetConnected()) {
            if (isWifiConnected()) {
                startNSDServiceDiscovery();
            }
            mInternetCheck = new InternetCheck(internet -> {
                if (internet) {
                    getDevices();
                } else {
                    Toast.makeText(getApplicationContext(), "Internet Not available",
                            Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            LoggerConfig.eLog(TAG, "Not connected to internet");
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerWifiStateChangeReceiver();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterWifiStateChangeReceiver();
    }

    @Override
    public void handleSuccessData(BaseModel resModel) {

        if (resModel != null) {
            if (resModel instanceof Res_GetDevices) {
                mDevicePinDataSingleton.setResGetDevices((Res_GetDevices) resModel);
                if (mDevicePinDataSingleton.getResGetDevices() != null &&
                        mDevicePinDataSingleton.getResGetDevices().getDevices().size() > 0) {
                    mNoDeviceLi.setVisibility(View.GONE);
                    initDevicesRecyclerView();
                    initDevicePinsRecyclerView(mDevicePinDataSingleton.getResGetDevices());

                } else {
                    mNoDeviceLi.setVisibility(View.VISIBLE);
                }

            } else if (resModel instanceof Res_UpdatePinState) {
                Res_UpdatePinState resUpdatePinState = (Res_UpdatePinState) resModel;
                if (resUpdatePinState.getResult() == 1) {
                    Toast.makeText(getApplicationContext(), "Updated!", Toast.LENGTH_SHORT).show();
                }

            }

        } else {
            LoggerConfig.eLog(TAG, "handleSuccessData:: resModel is null");
        }

    }

    private void initDevicesRecyclerView() {
        Adapter_Device adapterDevice = new Adapter_Device(this,
                mDevicePinDataSingleton.getResGetDevices().getDevices(),
                this);
        mDevicesRecyclerView.setAdapter(adapterDevice);
    }

    private void initDevicePinsRecyclerView(Res_GetDevices resGetDevices) {
        if (resGetDevices != null
                && resGetDevices.getDevices() != null
                && resGetDevices.getDevices().size() > 0
                && resGetDevices.getDevices()
                .get(mDevicePinDataSingleton.getSelectedDevicePos()).getPins() != null) {
            resGetDevices.getDevices()
                    .get(mDevicePinDataSingleton.getSelectedDevicePos())
                    .mIsDeviceSelected = true;
            onDeviceSelected(mDevicePinDataSingleton.getSelectedDevicePos(),
                    resGetDevices.getDevices().get(mDevicePinDataSingleton.getSelectedDevicePos()));
        } else {
            LoggerConfig.eLog(TAG,
                    "initDevicePinsRecyclerView:: Something is wrong with device data");
        }


    }

    @Override
    public void handleZeroData(BaseModel reqModel) {

    }

    @Override
    public void onValidationSucceeded() {

    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {

    }

    @Override
    public void onValidationFailure(int viewId, String message) {

    }

    @OnClick(R.id.no_device_li)
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.no_device_li:
                startActivity(new Intent(this, Activity_AddNewDevice.class));
                break;
        }
    }

    private void getDevices() {
        Controller_GetDevices controllerGetDevices = new Controller_GetDevices();
        Req_GetDevices reqGetDevices = new Req_GetDevices();
        reqGetDevices.setUserId(getUserId());
        controllerGetDevices.startFetching(this, reqGetDevices);
    }

    @Override
    public void onDeviceSelected(int position, Res_GetDevices.Device device) {

        mDevicePinDataSingleton.setSelectedDevicePos(position);

        if (device != null && device.getPins() != null && device.getPins().size() > 0) {
            mDevicePinsRecyclerView.setAdapter(new Adapter_DevicePin(this, device.getPins(),
                    this));
        } else {
            mDevicePinsRecyclerView.setAdapter(null);
            LoggerConfig.eLog(TAG, "onDeviceSelected:: Device data is empty/null");
        }

    }

    @Override
    public void onPinStateChanged(int position, boolean isOn, Res_GetDevices.Pin pin) {
        updatePinState(pin, isOn, position);
    }

    private void updatePinState(Res_GetDevices.Pin pin, boolean isOn, int position) {


        mDevicePinDataSingleton.getResGetDevices().getDevices()
                .get(mDevicePinDataSingleton.getSelectedDevicePos())
                .getPins().get(position)
                .setPinState(isOn ? Constants.ON : Constants.OFF);


        Controller_UpdateDeviceState controllerUpdateDeviceState = new Controller_UpdateDeviceState();
        Model_PinDetails modelPinDetails = new Model_PinDetails();
        modelPinDetails.setDeviceId(pin.getDeviceId());
        modelPinDetails.setPinName(pin.getPinName());
        modelPinDetails.setPinNo(pin.getPinNo());
        if (isOn) {
            modelPinDetails.setPinState(Constants.ON);
        } else {
            modelPinDetails.setPinState(Constants.OFF);
        }
        Req_UpdatePinState reqUpdatePinState = new Req_UpdatePinState();
        reqUpdatePinState.setModelPinDetails(modelPinDetails);
        controllerUpdateDeviceState.startFetching(this, reqUpdatePinState);
    }

}
