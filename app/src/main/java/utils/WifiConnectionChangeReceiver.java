package utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class WifiConnectionChangeReceiver extends BroadcastReceiver {

    private final String TAG = "WifiConnChangeReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        ConnectivityManager connectivityManager =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetInfo = connectivityManager.getActiveNetworkInfo();

        if (activeNetInfo != null
                && activeNetInfo.getType() == ConnectivityManager.TYPE_WIFI) {
            LoggerConfig.eLog(TAG,"onReceive:: wifi connected");
        } else {
            LoggerConfig.eLog(TAG,"onReceive:: wifi disconnected");
        }
    }
}
