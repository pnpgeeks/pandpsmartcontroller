package utils;

import android.content.Context;
import android.preference.PreferenceManager;

public class MySharedPreferences {

    private static String DEVICE_IMEI = "device_imei";
    private static String UCID = "ucid";



    public static String getDeviceIMEI(Context context){
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getString(DEVICE_IMEI,null);
    }

    public static void setDeviceIMEI(Context context, String deviceIMEI){
        PreferenceManager.getDefaultSharedPreferences(context).edit()
                .putString(DEVICE_IMEI,deviceIMEI).apply();

    }

    public static String getUCID(Context context){
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getString(UCID,null);
    }

    public static void setUCID(Context context, String deviceIMEI){
        PreferenceManager.getDefaultSharedPreferences(context).edit()
                .putString(UCID,deviceIMEI).apply();

    }


}
