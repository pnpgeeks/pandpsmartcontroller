package utils;

public class Constants {

    public static final String EXTRA_DEVICE_DETAILS_JSON = "extra_device_details_json";

    public static final int INTENT_FOR_SIGN_UP = 10002;

    public static final String PIN1 = "1";
    public static final String PIN2 = "2";
    public static final String PIN3 = "3";
    public static final String PIN4 = "4";


    public static final String ON = "1";
    public static final String OFF = "0";

    public static final String NOTIFY = "1";

    public static final String SPOKEN_TEXT_FOR_PIN1_ON = "Turn on pin 1";
    public static final String SPOKEN_TEXT_FOR_PIN1_OFF = "Turn off pin 1";
    public static final String SPOKEN_TEXT_FOR_PIN2_ON = "Turn on pin 2";
    public static final String SPOKEN_TEXT_FOR_PIN2_OFF = "Turn ff pin 2";
    public static final String SPOKEN_TEXT_FOR_PIN3_ON = "Turn on pin 3";
    public static final String SPOKEN_TEXT_FOR_PIN3_OFF = "Turn off pin 3";
    public static final String SPOKEN_TEXT_FOR_PIN4_ON = "Turn on fan";
    public static final String SPOKEN_TEXT_FOR_PIN4_OFF = "Turn off fan";


    public static final String FONT_BOLD = "product_sans_bold.ttf";
    public static final String FONT_BOLD_ITALIC = "product_sans_bold_italic.ttf";
    public static final String FONT_ITALIC = "product_sans_italic.ttf";
    public static final String FONT_REGULAR = "product_sans_regular.ttf";

    public static final int CAMERA_PERMISSION_REQUEST_CODE = 1001;
    public static final int MICROPHONE_PERMISSION_REQUEST_CODE = 1005;
    public static final int LOCATION_PERMISSION_REQUEST_CODE = 1002;
    public static final int REQUEST_CODE_SETTINGS = 1003;
    public static final int REQUEST_STORAGE_PERMISSION = 1004;

    public static final int LOCATION_PERMISSION_IN_SETTINGS = 10002;
    public static final int CAMERA_PERMISSION_IN_SETTINGS = 10003;
    public static final int MICROPHONE_PERMISSION_IN_SETTINGS = 10004;
    public static final int STORAGE_PERMISSION_IN_SETTINGS = 10005;

    public static final int INTENT_START_ACTIVITY_SCAN_PROMPT = 190;
    public static final int INTENT_START_ACTIVITY_SCAN_QR_CODE = 191;

    public static final String NSD_SERVICE_TYPE = "_esp._tcp.";
    public static final String NSD_SERVICE_NAME = "esp";

}
