package utils;


public class URLGenerator {

//    private static final String MAIN_URL = BuildConfig.SERVER_URL;

    public static final String BASE_URL = "http://ersnexus.esy.es/p&psmartcontroller/";

    public static final String TEST_API = BASE_URL + "APITest/APITest.php";

    public static final String API_SIGN_UP = BASE_URL + "pandp_register.php";

    public static final String API_SIGN_IN = BASE_URL + "panp_signin.php";

    public static final String API_ADD_NEW_DEVICE = BASE_URL + "pandp_add_device.php";

    public static final String API_GET_DEVICES = "pnp_get_devices.php";

    public static final String API_UPDATE_PIN_STATE = "pandp_update_pin_state.php";




}
