package utils;

import android.util.Log;

public class LoggerConfig {

    private static boolean ON = true;


    public static void eLog(String tag,String message){

        if(ON){
            Log.e(tag,message);
        }

    }

    public static void iLog(String tag, String message){

        if(ON){
            Log.i(tag,message);
        }
    }

}
