package utils;

import android.app.Activity;
import android.content.Context;

import androidx.appcompat.app.AlertDialog;
import pandp.co.R;


public abstract class DialogBox extends Activity {

    public static void showDialog(final Context context, String title, String message) {
        if (!((Activity) context).isFinishing()) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(context,R.style.MyDialogTheme);
            builder.setTitle(title);
            builder.setMessage(message);
            builder.setNegativeButton("Dismiss", (dialogInterface, i) ->
                    dialogInterface.dismiss());
            AlertDialog alertDialog = builder.create();
            alertDialog.show();
        }

    }
}