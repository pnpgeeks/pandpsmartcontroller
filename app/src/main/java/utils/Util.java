package utils;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.TimeZone;
import java.util.stream.Collectors;

import androidx.core.app.ActivityCompat;

public class Util {

    private static final String TAG = "Util";

    public static String getModel() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return model;
        }
        return manufacturer + " " + model;
    }

    public static String getOSVersion() {
        int currentApiVersion = Build.VERSION.SDK_INT;
        return String.valueOf(currentApiVersion);
    }

    /**
     * return IP Address of device
     */
    public static String getIpAddress() {
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface networkInterface : interfaces) {
                List<InetAddress> addresses = Collections.list(networkInterface.getInetAddresses());
                for (InetAddress inetAddress : addresses) {
                    if (!inetAddress.isLoopbackAddress()) {
                        String hostAddress = inetAddress.getHostAddress();
                        boolean isIPv4 = hostAddress.indexOf(':') < 0;
                        if (isIPv4)
                            return hostAddress;
                    }
                }
            }
        } catch (Exception ex) {
            LoggerConfig.eLog(TAG, ex.getMessage());
        }
        return "";
    }

    @SuppressLint("HardwareIds")
    public static void setDeviceIMEIInSharedPreference(final Context context) {

        TelephonyManager mTelephonyManager;

        String deviceId;

        mTelephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        if (ActivityCompat.checkSelfPermission(context,
                Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
            if (mTelephonyManager != null && mTelephonyManager.getDeviceId() != null) {
                deviceId = mTelephonyManager.getDeviceId();
            } else {
                deviceId = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
            }
            MySharedPreferences.setDeviceIMEI(context, deviceId);

        }


    }

    public static String getShortTimezone() {

        String timezone = TimeZone.getDefault().getDisplayName(false, TimeZone.LONG);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Arrays.stream(timezone.split(" "))
                    .map(s -> s.substring(0, 1))
                    .collect(Collectors.joining());
        } else {
            StringBuilder timeZoneInitials = new StringBuilder();
            String[] myName = timezone.split(" ");
            for (String s : myName) {
                timeZoneInitials.append(s.charAt(0));
            }

            return timeZoneInitials.toString();
        }

    }

}
