package singleton;

public class ControllerDataSingleton {

    private String mHostName;
    private String mServiceName;
    private String mServiceType;
    private String mPort;

    private static final ControllerDataSingleton ourInstance = new ControllerDataSingleton();

    public static ControllerDataSingleton getInstance() {
        return ourInstance;
    }

    private ControllerDataSingleton() {
    }



    public String getHostName() {
        return mHostName;
    }

    public void setHostName(String hostName) {
        mHostName = hostName;
    }

    public String getServiceName() {
        return mServiceName;
    }

    public void setServiceName(String serviceName) {
        mServiceName = serviceName;
    }

    public String getServiceType() {
        return mServiceType;
    }

    public void setServiceType(String serviceType) {
        mServiceType = serviceType;
    }

    public String getPort() {
        return mPort;
    }

    public void setPort(String port) {
        mPort = port;
    }

    public static ControllerDataSingleton getOurInstance() {
        return ourInstance;
    }
}
