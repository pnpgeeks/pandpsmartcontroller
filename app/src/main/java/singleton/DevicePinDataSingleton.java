package singleton;

import java.util.ArrayList;

import models.Res_GetDevices;

public class DevicePinDataSingleton {

    private final String TAG = "DevicePinDataSingleton";

    private static final DevicePinDataSingleton ourInstance = new DevicePinDataSingleton();

    private ArrayList<Res_GetDevices.Pin> mPins;
    private Res_GetDevices mResGetDevices;
    private int mSelectedDevicePos = 0;


    public static DevicePinDataSingleton getInstance() {
        return ourInstance;
    }

    private DevicePinDataSingleton() {
    }

    public int getSelectedDevicePos() {
        return mSelectedDevicePos;
    }

    public void setSelectedDevicePos(int selectedDevicePos) {
        mSelectedDevicePos = selectedDevicePos;
    }

    public Res_GetDevices getResGetDevices() {
        return mResGetDevices;
    }

    public void setResGetDevices(Res_GetDevices resGetDevices) {
        mResGetDevices = resGetDevices;
    }

    public ArrayList<Res_GetDevices.Pin> getDevicePins(){
        return mPins;
    }

    public void setDevicePins(ArrayList<Res_GetDevices.Pin> devicePins){
        mPins = devicePins;
    }

}
